// main.c
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int get_random(int min,int max) {
    int fd = open( "/dev/urandom", O_RDONLY );
    unsigned int result = 0;
    if( fd != -1 )
        read(fd, &result, sizeof(int) );
    close(fd);
    result=min+result%(max-min+1);
    return result;
}

int main (int argc, char *argv[]){
int i, balloon[i], stat, status;

	if (argc!=2) {
		printf("Usage: ./main <number>\n");
		exit(-1);
	}

int count = atoi (argv[1]);
for (i=0; i<count; i++){
	balloon [i] = fork();

	if (-1 == balloon[i]){
		perror ("fork");
		exit (-1);
	}
	else if (0==balloon[i]){
		
		int min = 0;
        int max = 200;
        int hor = 0, ver = 100;
        int nhor, nver, dx, dy;
        while (ver>5) {
        	nhor = get_random(min, max);
        	nver = get_random(min, max); 
        	dx = nhor - hor;
        	dy = nver - ver;
        	hor = hor+dx;
        	ver = ver+dy;
        	printf ("CHILD: Это шарик %d и я сместился на %d:%d и мои координаты %d:%d\n", i, dx, dy, hor, ver);
        }
        sleep(get_random(min, max)%4);
        printf ("CHILD: Это шарик %d и я лопнул\n", i);
        exit(0);
	}
}
printf("PARENT: This is parent process!\n");
	for (i = 0; i < count; i++) {
		status = waitpid(balloon[i], &stat, 0);
		if (balloon[i] == status){
			printf("Child Process %d done, result=%d\n", i, WEXITSTATUS(stat));
		}
	}
		
return 0;
}