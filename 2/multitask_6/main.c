/* 6. Шарики. Координаты заданного количества 
шариков изменяются на случайную величину 
по вертикали и горизонтали. При выпадении 
шарика за нижнюю границу допустимой области 
шарик исчезает. Изменение координат каждого
шарика в отдельном процессе (потоке). */

#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int get_random(int min,int max) {
    int fd = open( "/dev/urandom", O_RDONLY );
    unsigned int result = 0;
    if( fd != -1 )
        read(fd, &result, sizeof(int) );
    close(fd);
    result=min+result%(max-min+1);
    return result;
}

int main(int argc, char *argv[]) {
    int i, pid[argc], status, stat;
    
    if (argc < 2) {
        printf("Usage: <param1> <param2> e.t.c.\n");
        exit(-1);
    }

    for (i = 1; i < argc; i++) {
        pid[i] = fork();
        srand(getpid());

        if (-1 == pid[i]) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        } 

        else if (0 == pid[i]) {
            
            int min = 0;
            int max = 200;
            int hor = 0, ver = 100;
            int nhor, nver, dx, dy;
            	while (ver>5) {
                nhor = get_random(min, max);
                nver = get_random(min, max); 
                dx = nhor - hor;
                dy = nver - ver;
                hor = hor+dx;
                ver = ver+dy;
                printf ("CHILD: Это шарик %d и я сместился на %d:%d и мои координаты %d:%d\n", i, dx, dy, hor, ver);
           	}
            sleep(rand()%4);
            printf ("CHILD: Это шарик %d и я лопнул\n", i);
            exit(strlen(argv[i])); /* выход из процесс-потомока */
		}
    }
    // если выполняется родительский процесс
    printf("PARENT: Это процесс-родитель!\n");
    // ожидание окончания выполнения всех запущенных процессов
    for (i = 1; i < argc; i++) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done,  result=%d\n", i, WEXITSTATUS(stat));
        }
    }
    return 0;
}