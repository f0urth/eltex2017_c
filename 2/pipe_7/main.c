// main.c
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

int get_random(int min,int max) {
    int fd = open( "/dev/urandom", O_RDONLY );
    unsigned int result = 0;
    if( fd != -1 )
        read(fd, &result, sizeof(int) );
    close(fd);
    result=min+result%(max-min+1);
    return result;
}  // рандомизатор

int main (int argc, char *argv[]){
    int i, balloon[250], stat, status; //счетчик, массив шариков, переменные статуса
	if (argc!=2) {  // условие ошибки
		printf("Usage: ./main <number>\n");
		exit(-1);
	}

    int count = atoi (argv[1]);     //преобразование кол-ва шариков в цифру
    int fd[2];
    pid_t buff;
    pipe (fd);
    for (i=0; i<count; i++){
	
                      // создание канала fd
	balloon [i] = fork();        //создание системного вызова fork
    if (-1 == balloon[i]){       //ошибка при невозможности создания forka
		perror ("fork");
		exit (-1);
	}
    else if (0==balloon[i]){       //если форк создан: "поехали!"
		int min = 0, max = 200;       //параметры рандомизатора
        int hor = 0, ver = 100;         //начальные координаты
        int nhor, nver, dx, dy, k;      //инициализация новых переменных
        while (ver>5) {                //полет шарика
        	nhor = get_random(min, max);
        	nver = get_random(min, max); 
        	dx = nhor - hor;
        	dy = nver - ver;
        	hor = hor+dx;
        	ver = ver+dy;
        	printf ("CHILD: Это шарик %d. Сместился на %d:%d. Координаты %d:%d\n", i+1, dx, dy, hor, ver);
        }
        pid_t pid = getpid();
        printf("CHILD: my pid %d\n", pid);
        close (fd[0]);
        write (fd[1], &pid, sizeof(pid));
        sleep (1000);
        exit(0);
	}
}
sleep(2);
printf("PARENT: This is parent process!\n");
 close (fd[1]);
    while (read (fd[0], &buff, sizeof(pid_t))>0) {
    kill(buff, SIGKILL);
    printf("PARENT:I killed process %d\n", buff);
    } 
for (i = 0; i < count; i++) {
		status = waitpid(balloon[i], &stat, 0);
		if (balloon[i] == status){
			printf("Child Process %d done\n", i+1);
		}
    }
return 0;
}