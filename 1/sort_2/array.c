#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h> /* для mtrace */
#include <string.h>
#define MAX_LEN 1024 /* максимальная длина строки */

int inp_str(char *string, int maxlen) {
    char buffer[MAX_LEN];
    char bufflen[MAX_LEN];
    sprintf(bufflen, "%%%d[^\n]", maxlen);
    scanf(bufflen, buffer);
    getchar();
    strcpy(string, buffer);
    size_t len = strlen(buffer);
    return (int)len;

}

char** readMas(int count, char **mas, int maxlen){
	    for (int i = 0; i < count ; i++){
        mas[i] = (char *)malloc(sizeof(char)*maxlen); //выделяем память для строки
        inp_str(mas[i], maxlen);
    }    return mas; 
}

void printMas(char **mas, int count){
    for (int i = 0; i < count ; i++){
        printf("%s\n", mas[i]);
    }
}

void freeMas(char **mas, int count){
	for (int i = 0; i < count; i++){
        free(mas[i]); // освобождаем память для отдельной строки
    }
    free(mas); // освобождаем памать для массива указателей на строки
}

int getLenFirst(char *str) {
    
    int len = 0;
    for (int i=0; i < strlen(str); i++)
    {
        if (str[i]!='\n'){
            len++;
        } else {
            break;
        }
    }
    return len;
}

/*static int cmpstringp(const void *p1, const void *p2){
    

    int len1 = getLenFirst(*(char * const*)p1);
    int len2 = getLenFirst(*(char * const*)p2);
    return len2 - len1;


    //return strcmp(*(char * const*)p1, *(char* const*)p2);
} */

/* static int cmpstringp2(const void *p1, const void *p2){
    
    char *pp1, *pp2;
    char buff1[MAX_LEN];
    char buff2[MAX_LEN];
    //pp1 = malloc(sizeof(char)*strlen(*(char * const*)p1));
    //pp2 = malloc(sizeof(char)*strlen(*(char * const*)p2));
    // char buff[MAX_LEN];
    strcpy(buff1, *(char * const*)p1);
    strcpy(buff2, *(char * const*)p2);
    pp1 = strtok(buff1, " ");
    pp2 = strtok(buff2, " ");
    int len = strlen(pp1) - strlen(pp2);
    //free(pp1);
    //free(pp2);
    return strlen(pp1)-strlen(pp2);


    //return strcmp(*(char * const*)p1, *(char* const*)p2);
} */

int bubbleSort(char **mas, int n){

    int c = 0;
    int d = 0;
    char *swap;
    int swichcount = 0;
    for (c = 0 ; c < ( n - 1 ); c++)
  {
    for (d = 0 ; d < n - c - 1; d++)
    {
      if (getLenFirst(mas[d+1]) > getLenFirst (mas[d])){
        swap     = mas[d];
        mas[d]   = mas[d+1];
        mas[d+1] = swap;
        swichcount++;      
      }
    }
  }
return swichcount;
}

/*int CountWord(char **mas, int count) {
int words = 0;


    for (int i=0; i < strlen(str); i++)
    {
    if (str[i]=' '){
        words++;
        } else {
        break;
        }
    }
    return words;
}*/

int main(int argc, char **argv){

    char **mas = NULL;
    int count =3;
    char str;
    //mtrace();
    printf("Введите кол-во строк:");
    scanf ("%d", &count);
    getchar(str);
    //mas = (char **)malloc(sizeof(char *)*count);
    mas = readMas(count, mas, MAX_LEN);
    //qsort(mas, count, sizeof(char *), cmpstringp);
    //int swichcount = bubbleSort (mas, count);
    printMas (mas, count);
    //printf ("Кол-во перестановок:");
    //printf("%d\n",  swichcount);
    //freeMas (mas, count);
        //int words = CountWord(mas, count);
        //printf ("%d\n", words);
    int len = getLenFirst(str);
    printf ("%d\n", len);

}


//принтф домашка
//pastebin.com сайт с исходниками
/*6
Расположить строки по убыванию количества слов 
Входные параметры:
1. Массив 
2. Размерность массива 
Выходные параметры:
1. Количество перестановок 
2. Максимальное количество слов */

//подсчет кол-ва слов