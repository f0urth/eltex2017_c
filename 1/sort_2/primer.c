#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_LEN 1024 /* максимальная длина строки */

int inp_str(char *string, int maxlen) {
    char buffer[MAX_LEN];
    char bufflen[MAX_LEN];
    sprintf(bufflen, "%%%d[^\n]", maxlen);
    scanf(bufflen, buffer); // читаем строку в буфер
    getchar();
    strcpy(string, buffer); //копируем строку из буфера в массив указателей
    size_t len = strlen(buffer);
    return (int)len;
}

int inp_str2(char**string, int maxlen){
    char buffer[MAX_LEN];
    char bufflen[MAX_LEN];
    sprintf(bufflen, "%%%ds", maxlen);
    scanf(bufflen, buffer); // читаем строку в буфер
    size_t len = strlen(buffer);
    *string = (char *)malloc(sizeof(char)*len); //выделяем память для строки
    strcpy(*string, buffer); //копируем строку из буфера в массив указателей
    return (int)len;
} 
char** readMas(int count, char **mas, int maxlen){
    for (int i = 0; i < count ; i++){
        mas[i] = (char *)malloc(sizeof(char)*maxlen); //выделяем память для строки
        inp_str(mas[i], maxlen);
    }
    return mas; 
}

void printMas(char **mas, int count){
    for (int i = 0; i < count ; i++){
        printf("%s\n", mas[i]);
    }
}

void freeMas(char **mas, int count){
	for (int i = 0; i < count; i++){
        free(mas[i]); // освобождаем память для отдельной строки
    }
    free(mas); // освобождаем памать для массива указателей на строки
}

int getLenFirst(char *str){
    int len = 0;
    for (int i = 0; i < strlen(str); i++)
    {
        if (str[i]!='\n'){
            len++;
        } else{
            break;
        }
    }
    return len;
}

static int cmpstringp2(const void *p1, const void *p2){
    int len1 = getLenFirst(*(char * const*)p1);
    int len2 = getLenFirst(*(char * const*)p2);
    return len2 - len1;
}

static int cmpstringp(const void *p1, const void *p2){
    char *pp1, *pp2;
    char buff1[MAX_LEN];
    char buff2[MAX_LEN];
    strcpy(buff1, *(char * const*)p1);
    strcpy(buff2, *(char * const*)p2);
    pp1 = strtok (buff1, " ");
    pp2 = strtok (buff2, " ");
    int len = strlen(pp2)- strlen(pp1);
    return len;
}

int bubleSort(char **mas, int n){
 int c = 0;
 int d = 0;
 int swichcount = 0;
 char *swap;
  for (c = 0 ; c < ( n - 1 ); c++){
    for (d = 0 ; d < n - c - 1; d++){
      if (getLenFirst(mas[d+1]) > getLenFirst(mas[d])){
        swap     = mas[d];
        mas[d]   = mas[d+1];
        mas[d+1] = swap;
        swichcount++;
      }
    }
  }
  return swichcount;
}

int main(int argc, char **argv){
	char **mas = NULL; //указатель на массив указателей на строки
	int count = 3;
	//mtrace();
    printf("Введите кол-во строк:");
    scanf("%d", &count);
    getchar();
    mas = (char **)malloc(sizeof(char *)*count);// выделяем память для массива указателей
	mas = readMas(count, mas, MAX_LEN);
   // qsort(mas, count, sizeof(char *), cmpstringp2);
    int swichcount = bubleSort(mas, count);

    //int len = getLenFirst(str);
    //int len = inp_str(string, maxlen);
	printMas(mas, count);
    printf("Кол-во перестановок:");
    printf("%d\n", swichcount);
	freeMas(mas, count);
}