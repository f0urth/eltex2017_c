#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_LEN 1024

int WordCounter (char *);

int inp_str(char *string, int maxlen) {
    char buffer[maxlen];
    char bufflen[maxlen];
    sprintf(bufflen, "%%%d[^\n]", maxlen);
    scanf(bufflen, buffer); // читаем строку в буфер
    getchar();
    strcpy(string, buffer); //копируем строку из буфера в массив указателей
    size_t len = strlen(buffer);
    return (int)len;
}

char** readMas(int count, char **mas, int maxlen) {
    for (int i = 0; i < count ; i++){
        mas[i] = (char *)malloc(sizeof(char)*maxlen); //выделяем память для строки
        inp_str(mas[i], maxlen);
        int cnt=WordCounter(mas[i]);
    }
    return mas; 
}

int WordCounter (char *string) {
    int i, count;
    for (i=0, count=0; string[i]; i++)
        count += (string[i] == ' ');
    count++;
    return count;
}

int bubbleSort(char **mas, int n, int *x){
    int c = 0;
    int d = 0;
    int switchcount = 0;
    char *swap;
        for (c = 0 ; c < ( n - 1 ); c++){
            for (d = 0 ; d < n - c - 1; d++){
                if (WordCounter(mas[d+1]) > WordCounter(mas[d])){
                    swap     = mas[d];
                    mas[d]   = mas[d+1];
                    mas[d+1] = swap;
                    switchcount++;
                    //*x=mas[d+1];        
                }
            }

        }
    *x=WordCounter(mas[0]);
    return switchcount;
}

void printMas(char **mas, int count){
    for (int i = 0; i < count ; i++){
        printf("%s\n", mas[i]);
        }
} 

void freeMas(char **mas, int count){
	for (int i = 0; i < count; i++){
        free(mas[i]); // освобождаем память для отдельной строки
    }
    free(mas); // освобождаем памать для массива указателей на строки
}

int main (int argc, char **argv){

	char **mas = NULL;
	int count = 0;
    int x =0;
	printf("Введите кол-во записей:");
	scanf ("%d\n", &count);
	mas = (char**) malloc (sizeof (char*) * count);
	mas = readMas(count, mas, MAX_LEN);
	int switchcount=bubbleSort(mas, count, &x);
    printMas (mas, count);
    printf("%s%d\n", "Количество перестановок:", switchcount);
    printf("%d\n", x);
	freeMas(mas, count);
}