#include <stdio.h>
#include <stdlib.h>

struct team {
	char teamname [20];
	int score;
	int summ;
};

typedef struct team team;

void readTeam(team *criterions) {
	printf("Введите название команды:");
	scanf("%s", criterions->teamname);
	printf("Введите счет команды:");
	scanf("%d", &criterions->score);
	printf ("Введите сумму приза:");
	scanf("%d", &criterions->summ);
}

static int cmpfunc (const void *p1, const void *p2) {
	team *par1= *(team**)p1;
	team *par2= *(team**)p2;
	return par1->summ-par2->summ; 
}
	

void printeach (team *each) {
	printf("Команда: %s\n", each->teamname);
	printf("Счет: %d\n", each->score);
	printf("Сумма приза: %d\n", each->summ);
}

void printTeam (team **mas, int count) {
	for (int i = 0; i<count; i++){
		printeach(mas[i]);
	}
}

int main (int argc, char **argv) {

	int count = 0;
	printf ("Кол-во записей:");
	scanf ("%d", &count);
	//getchar ();
	team **mas;

	mas = (team**) malloc(sizeof(team*)* count);

	for (int i = 0; i<count; i++) {
		mas [i] = (team*)malloc(sizeof(team));
		readTeam(mas[i]);
	}
	qsort (mas, count, sizeof (team*), cmpfunc);
	printTeam(mas, count);
	for (int i=0; i<count; i++) {
		free (mas[i]);
	}
	free(mas);
	return (EXIT_SUCCESS);
}


/* Расположить записи в порядке возрастания по сумме призового фонда */