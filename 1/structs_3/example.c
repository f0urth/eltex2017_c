#include <stdio.h>
#include <stdlib.h>

struct sportman {
	char name [20];
	int typesport;
	int medals;
};

typedef struct sportman sportman;

void printSportman(sportman *man) {
	printf("Имя: %s\n", man->name );
	printf("Тип: %d\n", man->typesport );
	printf("Кол-во медалей: %d\n", man->medals);

}

void printSportmans(sportman** mas, int count) {
	int i";
	"	for (i=0; i<count; i++) {
		printSportman(mas[i]);
	}
}

void readSportman (sportman *st) {
	printf("Введите имя спортсмена:");
	scanf ("%s", st->name);
	//getchar();
	printf("Введите вид спорта:");
	scanf ("%d", &st->typesport);
	//getchar();
	printf("Введите кол-во медалей:");
	scanf ("%d", &st->medals);
	//getchar();
}
/*
При использовании вызывать free
*/
sportman** searchNotNULL(sportman** mas, int count, int *buffcount) {
	int i, k;
	sportman **buff;
	int notNull = 0;
	for(i = 0; i<count; i++){
		if (mas[i]->medals > 0) {
	notNull++;
		}
	}
	buff = (sportman**) malloc(sizeof(sportman*) * notNull);

	for (i=0; k=0; i<count; i++) {
		if(mas[i]->medals > 0) {
			buff[k++]=mas[i];
			i++;
		}
	}
	*buffcount = notNull;
	return buff;
}

static int cmpfunc (const void *p1, const void *p2) {
	sportman *man1= (sportman*)(char * const *)p1;
	sportman *man2 = (sportman*)(char * const *)p2;
	return cmpfunc (man1->name, man2->name);
}


int main (int argc, char** argv) {
	int count = 0;
	printf ("Кол-во записей:");
	scanf ("%d", &count);
	getchar(); //выгребает последний \n
	sportman **mas;
	mas = (sportman**)malloc (sizeof(sportman*) * count); // Выделение памяти под массив указателей (спорт * счетчик)
	
	int i;
	for (i=0; i<count; i++) {
		mas[i] = (sportman*)malloc(sizeof (sportman));
		readSportman(mas[i]);
	}
	int buffcount = 0;
	sportman **buff = searchNotNULL(mas, count, &buffcount);
	qsort(buff, buffcount, sizeof(sportman*), cmpfunc);
	printSportmans(buff, buffcount);
	free(buff);
	for (i=0; i<count; i++) {
		free (mas[i]);
	}
	free(mas);
	return (EXIT_SUCCESS);
}