/* 6
После каждой точки вставить символ ‘\n’
Параметры командной строки:
	1. Имя входного файла 
	2. Количество вставок */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* int checkpoint (char buff[]) {    //передаем массив, в котором лежит строка
	int i;
	for (i=0; buff[i]!='\0'; i++){     //проверяем каждый элемент массива
		
		if (buff[i]='.') {   //  сравниваем с цифрой
				buff[i]='\n';
			}  
	}
	return 1;
} */

int readchars (FILE* fin, FILE* fout) {
	char ch;
	while((ch=fgetc(fin)) != EOF){
		if (ch =='.') { 
			ch = '\n'; 
		}
			
	fputc(ch, fout);
	}	

}
int main (int argc, char* argv[]) {

	if (argc<2) {
		fprintf(stderr, "Use <filename.in>.\n");
		exit (1);
	}

	char* name_out=(char*) malloc (sizeof(char)*strlen(argv[1])+2);
	strncpy(name_out, argv[1], strlen(argv[1])-3);
	strcat(name_out, ".out\0");
	printf ("%s created!\n", name_out);

	FILE *fin=fopen(argv[1], "r");
	FILE *fout=fopen(name_out, "w");

	if ((fin=fopen(argv[1], "r") )==NULL) {
		printf("Cannot open file for reading.\n");
		exit (1);
	}

	if (fout==NULL){
		printf("Cannot open file for writing.\n");
		exit (1);
	}
	readchars (fin, fout);
	fclose(fin);
	fclose(fout);
	free (name_out);
	return 0;

}

/* ЗАДАНИЕ:
В лабораторной работе требуется написать две программы для 
обработки текстовых файлов. Одна из них выполняет построчную, 
другая посимвольную обработку:
1. Написать программу, обрабатывающую текстовый файл и 	
записывающую обработанные данные в файл с таким же именем, 
но 	с другим типом (табл. 3).

2.Написать программу, выполняющую посимвольную обработку текстового файла (табл. 4). 

Ввод параметров должен быть организован в командной строке запуска программы. 
Исходный файл должен быть создан с помощью любого текстового редактора. 
При обработке текста рекомендуется использовать функции из 
стандартной библиотеки СИ для работы со строками, 
преобразования и анализа символов.
6
Оставить строки, не содержащие цифры 
Параметры командной строки:
1. Имя входного файла 
2. Количество обрабатываемых строк */


